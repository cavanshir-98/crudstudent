package az.firstProject.crud.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Mconfig {

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

    }

