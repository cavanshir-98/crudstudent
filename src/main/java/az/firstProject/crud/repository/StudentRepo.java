package az.firstProject.crud.repository;

import az.firstProject.crud.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepo extends JpaRepository<Student,Long> {
}
