package az.firstProject.crud.service;

import az.firstProject.crud.dto.StudentDto;
import az.firstProject.crud.model.Student;
import az.firstProject.crud.repository.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepo data;
    private final ModelMapper mapper;

    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student map = mapper.map(dto, Student.class);
        Student save = data.save(map);
        StudentDto save2 = mapper.map(save, StudentDto.class);
        return save2;
    }

    @Override
    public StudentDto getStudentById(Long id) {
        Student byId = data.getById(id);
        StudentDto map1 = mapper.map(byId, StudentDto.class);
        return map1;

    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student student = data.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Telebe tapilmadi")));
        student.setName(dto.getName());
        student.setBirthdate(dto.getBirthdate());
        student.setInstitute(dto.getInstitute());
        Student save = data.save(student);
        return mapper.map(save, StudentDto.class);

    }

    @Override
    public void deleteStudentById(Long id) {
        data.deleteById(id);
    }

    @Override
    public List<StudentDto> findAllStudent() {

         List<Student> all = data.findAll();
        List<StudentDto> map=all.stream().map(a->mapper.map(a,StudentDto.class)).collect(Collectors.toList());

        return map;
    }


}
