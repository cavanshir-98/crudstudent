package az.firstProject.crud.service;

import az.firstProject.crud.dto.StudentDto;
import az.firstProject.crud.model.Student;

import java.util.List;

public interface StudentService {

    StudentDto createStudent(StudentDto dto);

    StudentDto getStudentById(Long id);

    StudentDto updateStudent(StudentDto dto);

    void deleteStudentById(Long id);

    List<StudentDto> findAllStudent();




}
