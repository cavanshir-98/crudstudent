package az.firstProject.crud.controller;

import az.firstProject.crud.dto.HelloUser;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping
    public String sayHelloWhileUser(@RequestBody HelloUser user){
        return "Hello "+ user.getName();
    }

}